PImage acelerador;
PImage freno;
PImage freno_off;
PImage freno_on;
PImage indic_vel0;
PImage indic_vel1;
PImage indic_vel2;
PImage indic_vel3;
PImage indic_vel4;
PFont font;
float velocidad = 0;
int r_velocidad;
String s_velocidad;

void setup() {
  size(600,400);
  acelerador = loadImage("acelerador.png"); // se añade la imagen del acelerador
  freno = loadImage("freno.png"); //se añade la imagen del freno
  freno_off = loadImage("luz_freno2.png"); // luz de freno apagada
  freno_on = loadImage("luz_freno1.png"); // luz de freno prendida
  indic_vel0 = loadImage("indic_vel0.png"); // indicador velocidad 0
  indic_vel1 = loadImage("indic_vel1.png"); // indicador velocidad <1/4
  indic_vel2 = loadImage("indic_vel2.png");// indicador velocidad entre 1/4 y 1/2
  indic_vel3 = loadImage("indic_vel3.png"); // indicador velocidad entre 1/2 y 3/4
  indic_vel4 = loadImage("indic_vel4.png");  // indicador velocidad mas de 3/4
  font = createFont("Georgia", 50); // se añade la fuente del velocimetro
  textFont(font);
}

void draw() {
  background(204);
  image(acelerador, 0, 200); //se posiciona la imagen del acelerador
  image(freno, 400, 200); //se posiciona la imagen del freno
  image(freno_off, 218, 200); // se posiciona la luz de freno apagada
  image(indic_vel0, 420, 10); // se posiciona indicador velocidad cero
  r_velocidad = round(velocidad);
  s_velocidad = str(r_velocidad); // se generea el string del velocimetro
  text(s_velocidad, 295, 50); // se añade el velocimetro
  delay(400); // retardo del bucle del sistema
  //-------------------------------------------//
  //--- Indicadores de colores velocidad---//
   if(r_velocidad > 0 && r_velocidad < 45){
    image(indic_vel1, 420, 10);
  }
  if(r_velocidad >= 45 && r_velocidad < 90){
    image(indic_vel2, 420, 10);
  }
  if(r_velocidad >= 90 && r_velocidad < 135){
    image(indic_vel3, 420, 10);
  }
  if(r_velocidad >= 135){
    image(indic_vel4, 420, 10);
  }
  //-----------------------------------//
   if (mouseY > 200 && mouseX < 200){ // funcionalidad del acelerador
    if(velocidad < 180 && velocidad+1+(0.05*velocidad)<180){
      velocidad += 1+(0.05*velocidad);
      } else{
      velocidad = 180;}
    }
  else if (mouseY > 200 && mouseX > 400) { // funcionalidad del freno
    image(freno_on, 218, 200); // se enciende la luz del freno
    if(velocidad > 0 && velocidad-(1+0.1*velocidad)>0){
      velocidad = velocidad-(1+0.1*velocidad);
    } else{
    velocidad = 0;}
    }
  else{if(velocidad > 0 && velocidad - 2>0){ // cuando no se tiene el mouse ni en el acelerador ni en el freno
      velocidad = velocidad - 2;
    } else{
    velocidad = 0;}
  }
}
